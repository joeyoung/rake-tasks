##
# 	SqlServer Task
#
#	Required Variables:
#		DATABASE_NAME		- Name of the Database
#		DATABASE_SERVER 	- Server and instance of the database "(local)\\SQL2008"
#		DATABASE_USER		- Name of the user to create for the database
#		DATABASE_USERPASS	- Password of the application user 
#
#	Dependencies
#		osql should be in your path


require_relative 'utils.rb'

OSQL = "osql -E "

namespace :sqlserver do
	def create_login(login, password)
		sql = "IF NOT EXISTS(SELECT * FROM sys.server_principals WHERE name = '#{login}')
				BEGIN
					CREATE LOGIN [#{login}] WITH PASSWORD='#{password}', CHECK_POLICY=OFF
				END"
		
		result = system "#{OSQL} -S \"#{DATABASE_SERVER}\" -Q \"#{sql}"

		if result
			puts "#{login} login created on #{DATABASE_SERVER}".green 
		else
			puts "cannot create #{login} login on #{DATABASE_SERVER}".yellow
		end
	end

	def drop_login(login)
		sql = "IF EXISTS(SELECT * FROM sys.server_principals WHERE name = '#{login}')
				BEGIN
					DROP LOGIN #{login}
				END"
		result = system "#{OSQL} -S \"#{DATABASE_SERVER}\" -Q \"#{sql}"
		
		if result
			puts "#{login} dropped from #{DATABASE_SERVER}".green
		else
			puts "cannot drop #{login} from #{DATABASE_SERVER}".yellow
		end
	end

	def drop_database(database)
		sql = "IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'#{database}')
				BEGIN
					DROP DATABASE #{database}
				END"
		
		result = system "#{OSQL} -S \"#{DATABASE_SERVER}\" -Q \"#{sql}"
		
		if result
			puts "#{database} dropped from #{DATABASE_SERVER}".green
		else
			puts "cannot drop #{database} from #{DATABASE_SERVER}".yellow
		end
	end

	def create_database(database, dbuser)
		database_sql = "IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'#{database}')
						BEGIN
							CREATE DATABASE #{database}
						END"
		
		user_sql = "IF NOT EXISTS(SELECT * FROM sys.database_principals WHERE name = '#{dbuser}')
					BEGIN
						CREATE USER [#{dbuser}] FOR LOGIN [#{dbuser}] WITH DEFAULT_SCHEMA=[dbo]
						GRANT ALTER ON SCHEMA::dbo TO #{dbuser}
						GRANT CREATE SCHEMA, CREATE VIEW, CREATE TABLE, SELECT, UPDATE, INSERT, DELETE, REFERENCES TO #{dbuser}
					END"
					
		result = system "#{OSQL} -S \"#{DATABASE_SERVER}\" -Q \"#{database_sql}\""
		if result
			puts "#{database} created on #{DATABASE_SERVER}".green
		else
			puts "cannot create #{database} on #{DATABASE_SERVER}".yellow
		end
		
		result = system "#{OSQL} -S \"#{DATABASE_SERVER}\" -d #{database} -Q \"#{user_sql}"
		if result
			puts "#{dbuser} created on #{DATABASE_SERVER} for #{database}".green
		else
			puts "cannot create #{dbuser} on #{DATABASE_SERVER} for #{database}".yellow
		end
	end
	
	desc "Creates the Database"
	task :create do 
		begin
			create_login(DATABASE_USER, DATABASE_USERPASS)
			create_database(DATABASE_NAME, DATABASE_USER)
		rescue StandardError => error
			puts "Creating Database failed:".red
			puts error.to_s.red
		end
	end
			
	desc "Drops the Database"
	task :drop do
		begin
			drop_login(DATABASE_USER)
			drop_database(DATABASE_NAME)
		rescue StandardError => error
			puts "Dropping Database failed:".red
			puts error.to_s.red
		end
	end
end