##
# 	MSBuild Task
#
#	Required Variables:
#		SOLUTION - The path to the solution file 
#
#	Required Gems:
#		albacore

require "albacore"

namespace :build do
	desc "Initializes the build"
	task :init => :clean do
		#do nothing right now
	end

	desc "Cleans the build"
	task :clean do
		#do nothing right now
	end

    desc "Runs the debug build"
    msbuild :debug => :init do |msb|
        msb.properties :configruation => :Debug
        msb.targets :Clean, :Build
        msb.verbosity = "detailed"
        msb.solution = SOLUTION
    end
    
    desc "Runs the release build"
    msbuild :release => :init do |msb|
        msb.properties :configruation => :Release
        msb.targets :Clean, :Build
        msb.verbosity = "minimal"
        msb.solution = SOLUTION
    end
end