##
# 	Migrations Task
#
#	Required Variables:
#		MIGRATION_RUNNER 	- Full path to the migration runner (Migrate.exe)
#		MIGRATION_ASSEMBLY 	- Full path to the assembly containing the migations
#		MIGRATION_CONNECTIONSTRING	-	Connection String to use for the migration
#		MIGRATION_PROVIDER	- Provider for the migration (sql2008, mysql etc.)	
#

require_relative "utils.rb"

namespace :migrate do
	desc "Migrate the database up"
	task :up do
		begin
			sh "#{MIGRATION_RUNNER} -t migrate -a #{MIGRATION_ASSEMBLY} -db #{MIGRATION_PROVIDER} -connection \"#{MIGRATION_CONNECTIONSTRING}\""
		rescue StandardError => error
			puts "Migrate Database failed:".red
			puts error.to_s.red
		end
	end
		
	desc "Rollback the latest migration"
	task :rollback do
		begin
			sh "#{MIGRATION_RUNNER} -t rollback -a #{MIGRATION_ASSEMBLY} -db #{MIGRATION_PROVIDER} -connection \"#{MIGRATION_CONNECTIONSTRING}\""
		rescue StandardError => error
			puts "Rollback Migration failed:".red
			puts error.to_s.red
		end
	end
end