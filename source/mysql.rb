##
# 	MySql Task
#
#	Required Variables:
#		DATABASE_SERVER		- Server name where the database resides
#		DATABASE_NAME		- Name of the Database
#		DATABASE_USER		- Name of the application user to create for the database
#		DATABASE_USERPASS	- Password of the application user 
#
# 	Notes:
# 		Requires an options file to house your mysql security credentials
#		See the mysql_config_editor for more information http://dev.mysql.com/doc/refman/5.6/en/mysql-config-editor.html
#		The login-path must be named local so that mysql --login-path=local is valid

require_relative 'utils.rb'

MYSQL = "mysql --login-path=local -e"

namespace :mysql do
	desc "Creates the Database"
	task :create do 
		begin
		
			#create the database
			database_sql = "CREATE DATABASE IF NOT EXISTS \"#{DATABASE_NAME}\""
			result = system "#{MYSQL} \"#{database_sql};\""
			
			if result
				puts "#{DATABASE_NAME} created on #{DATABASE_SERVER}.".green
			else
				puts "cannot create #{DATABASE_NAME} on #{DATABASE_SERVER}.".yellow
			end
			
			#create the application user
			user_sql = "GRANT CREATE, ALTER, INDEX, SELECT, UPDATE, INSERT, DELETE ON #{DATABASE_NAME}.* TO '#{DATABASE_USER}'@'#{DATABASE_SERVER}' identified by '#{DATABASE_USERPASS}'"
			result = system "#{MYSQL} \"#{user_sql}\";"
			
			if result
				puts "#{DATABASE_USER} created on #{DATABASE_SERVER} for #{DATABASE_NAME}".green
			end
				puts "cannot create #{DATABASE_USER} on #{DATABASE_SERVER} for #{DATABASE_NAME}".yellow
			else
			
		rescue StandardError => error
			puts "Create Database failed:".red
			puts error.to_s.red
		end
	end
                        
	desc "Drops the Database"
	task :drop do
		begin
		
			sql = "DROP DATABASE IF EXISTS #{DATABASE_NAME}"
			result = system "#{MYSQL} \"#{sql}\";"
			
			if result
				puts "#{DATABASE_NAME} dropped on #{DATABASE_SERVER}".green
			else
				puts "cannot drop #{DATABASE_NAME} on #{DATABASE_SERVER}.".yellow
			end
			
		rescue StandardError => error
			puts "Dropping Database failed:".red
			puts error.to_s.red
		end
	end
end