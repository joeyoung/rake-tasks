require "fileutils"
require "net/http"
require "win32console"
require "io/console"

def create_folder(path)
	mkdir(path) unless File.directory?(path)
end

def delete_folder(path)
	rm_rf(path)
end

def delete_file(file)
	if File.exist?(file)
		FileUtils.rm( file, :noop => false, :verbose => false)
	end
end

def current_directory
  File.dirname(__FILE__)
end

def get_hidden_input(prompt)
	print prompt
	input = STDIN.noecho(&:gets).chomp
	return input
end

class NilClass
  def is_empty?
    true
  end
end

class String
  { :reset          =>  0,
    :bold           =>  1,
    :dark           =>  2,
    :underline      =>  4,
    :blink          =>  5,
    :negative       =>  7,
    :black          => 30,
    :red            => 31,
    :green          => 32,
    :yellow         => 33,
    :blue           => 34,
    :magenta        => 35,
    :cyan           => 36,
    :white          => 37,
  }.each do |key, value|
    define_method key do
      "\e[#{value}m" + self + "\e[0m"
    end
  end
  
  def is_empty?
	empty?
  end
  
  def is_not_empty?
	!empty?
  end
  
end
